# Example Voting App

Eine einfache App für eine Abstimmung mit zwei Resultaten. Die einzelnen Funktionen wurden auf Services aufgeteilt. 

## Architektur

Die Applikation besteht aus folgenden Komponenten:

- Eine Frontend-Web-App in Python, die es Ihnen ermöglicht, zwischen zwei Optionen zu wählen.
- Ein Redis, der neue Stimmen sammelt
- Ein .NET Worker, der Stimmen konsumiert und in einer…
- Postgres Datenbank speichert, die von einem Docker-Volume unterstützt wird
- Eine Node.js Web-App, die die Abstimmungsergebnisse in Echtzeit anzeigt.

![Architektur](architektur.png)

### Vote-Service

Der Vote-Service ist eine Paython-Anwendung und befindet sich im Verzeichnis vote. Folgende Schritte sind eroderlich, um die Applikation zu starten:
```
pip install --no-cache-dir -r requirements.txt

gunicorn app:app -b 0.0.0.0:80 --log-file - --access-logfile - --workers 4 --keep-alive 0
```
Der Vote-Service speichert die Stimme in einer Redis Datenbank,


### Result-Service

Der Result-Service ist eine NodeJS-Anwendung und befindet sich im Verzeichnis Result. 

#### Anwendung starten

Folgende Schritte sind eroderlich, um die Applikation zu starten:

*Abhänigkeiten Installieren*
```
npm install -g nodemon
npm ci
npm cache clean --force
```
*Applikation starten*
```
node server.js
```

#### Abhänigkeiten zu anderen Diensten

Der Vote-Service erfordert eine Verbindung zum Redis-Server. Dieser sucht er unter dem Host _redis_:

```
# app.py / Zeile 19 - 22
def get_redis():
    if not hasattr(g, 'redis'):
        g.redis = Redis(host="redis", db=0, socket_timeout=5)
    return g.redis
```

### Worker-Service

Der Worker-Service nimmt die Abstimmungen vom Redis-Service entgegen und speichert diese in der Datenbank. Es handelt sich um eine .NET Applikation. Dem Service liegt bereits ein Dockerfile bei, welches die Umwandlung in einen Container erlaubt.

#### Abhänigkeiten zu anderen Diensten

Der Worker-Service sucht nach einer Datenbank unter dem Hostnamen db. Als Benutzer-Name und Password wird jeweils hardcoded postgres verwendet. Der Datanabnk-Service wird unter dem Postgres Standard-Port 5432 gesucht, da kein expliziter Port gesetzt wurde:

```
# Program.cs / Zeile 19
var pgsql = OpenDbConnection("Server=db;Username=postgres;Password=postgres;");
```

Weiter sucht der Worker-Service eine Redis Instanz. Diese läuft auf dem Standard-Port:

```
# Program.cs / Zeile 20 - 21
var redisConn = OpenRedisConnection("redis");
var redis = redisConn.GetDatabase();
```

### Redis-Service

Es wird eine Redis Instanz ohne Passwort benötigt. Der Vote-Service versucht sich per Localhost auf den Redis-Standard-Port zu verbinden.


### Datenbank-Service

Es wird eine Postgres Datenbank benötigt. Folgende ENV-Variablen unterstützt das offizielle Postgres Image:

```
POSTGRES_USER
POSTGRES_PASSWORD
```

## Möglichkeiten die Anwendung auszubauen

- Healthchecks auf den Container einbauen: https://lumigo.io/container-monitoring/docker-health-check-a-practical-guide/
- Hardcoded Zugangsdaten durch ENV-Variablen ersetzen
